-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jun 2022 pada 08.53
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sansbook`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `book`
--

CREATE TABLE `book` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_book` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_book` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year_book` int(11) NOT NULL,
  `book_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_book` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `book`
--

INSERT INTO `book` (`id`, `name_book`, `author_book`, `year_book`, `book_type`, `description`, `image_book`, `link`, `deleted_at`, `created_at`, `updated_at`) VALUES
(19, '123', 'asd', 213, '123', 'qwe', 'qwe', 'qwe', '2022-06-21 16:03:33', '2022-06-21 16:01:27', '2022-06-21 16:03:33'),
(20, '1231231231', '123123123123', 121, '12123123123', '1231231233123', '213123231231231231231231231231', '1231231231231231231231231231231', '2022-06-21 16:03:42', '2022-06-21 16:03:22', '2022-06-21 16:03:42'),
(21, 'PENGEMBANGAN TEKNOLOGI PENDIDIKAN DAN PERANAN PENDIDIK DALAM MENGGUNAKAN MEDIA PEMBELAJARAN', 'ALWI HILIR, S.Kom., M.Pd', 2021, 'Teknologi', 'Di masa-masa ke depan, arus informasi akan makin makin makin melalui jaringan internet yang bernet global di seluruh dunia dan matilah untuk disingkupi dengan cara itu kalau tak mau ketinggalan jaman. Dengan di mana arah maka pendidikan khususnya proses proses cepat atau lambat tak terlepas dari komputer dan internet jadi bantu utama.', 'https://covers.zlibcdn2.com/covers299/books/a6/e5/fd/a6e5fd1e76c09347e29327ea5d37ce7c.jpeg', 'https://p302.zlibcdn.com/dtoken/0aae2cff96e8b83bb7dd1eff2dfcc2e4', NULL, '2022-06-21 16:06:51', '2022-06-21 16:06:51'),
(22, 'Metode Penelitian Kuantitatif, Kualitatif, dan Penelitian Gabungan', 'Muri Yusuf', 2017, 'Teknologi', 'Penelitian kuantitatif dipandang sebagai sesuatu yang bersifat konfirmasi dan deduktif, sedangkan penelitian kualitatif bersifat eksploratoris dan induktif, mixed methods merupakan paduan dari kuantitatif dan kualitatif. Beberapa aspek dibandingkan secara konsepsional yang membedakan ketiga jenis penelitian ini.', 'https://covers.zlibcdn2.com/covers299/books/a1/da/30/a1da30f8c4d7ab152a4e48ec7a1574a1.jpg', 'https://bunker2.zlibcdn.com/dtoken/342aca745b2b2e5e77642c2c46de87d9', NULL, '2022-06-21 16:25:14', '2022-06-21 16:25:14'),
(23, 'Si Juki: Komik Strip', 'Faza Meonk', 2014, 'Komik', 'Juki, karakter komik yang mengaku antimainstream dan merasa ngetop, memulai karirnya dari ‘bawah’. Awalnya, dia sekadar tampil sebagai cameo di komik-komik online karya Faza Meonk. Namun, karakternya yang lucu, ngeselin, cuek, dan banyak hoki itu membuat Juki semakin dikenal dan disukai pembacanya.Dalam komik ini terangkum perjalanan si juki sang aktor komik. Mulai dari digambar asal-asalan dan tidak bernama, kritik kocak dan kritisnya pada sekitar, memenangi penghargaan, sampai nekat mencalonkan diri menjadi presiden.Yuk, ikuti dan tertawa bersama rekaman perjalanan Juki.', 'https://covers.zlibcdn2.com/covers299/books/9c/4c/03/9c4c0320751e45bae678173b29b3c10a.png', 'https://p300.zlibcdn.com/dtoken/202731838921a81ea1bd53ff1e6edd4f', NULL, '2022-06-21 16:31:47', '2022-06-21 16:31:47'),
(24, 'Jack Ma & Alibaba', 'Yan Qicheng', 2018, 'Business & Economics - E-Commerce', 'Sekarang ini Jack Ma (Ma Yun) adalah salah satu raksasa dalam dunia internet. Perusahaannya, Alibaba, telah menjadi platform e-commerce terbesar di dunia. Berkembangnya Jack Ma dan Alibaba merupakan bagian integral dari majunya perekonomian internet Tiongkok. Buku ini mengisahkan pengalaman hidup Jack Ma, kariernya sebagai pengusaha dan proses keseluruhan didirikannya Alibaba. Dengan berada di balik layar, penulis menunjukkan bagaimana Jack Ma menumbangkan tradisi dan membangun Alibaba hingga menjadi raksasa, yang memecahkan rekor dunia sebagai penawaran saham publik terbesar di dunia saat IPO Alibaba berlangsung di New York di tahun 2014.', 'https://covers.zlibcdn2.com/covers299/books/f3/b7/10/f3b710eea4c037781ef96f6cf955df14.jpg', 'https://p302.zlibcdn.com/dtoken/f553e3d1c658ba0bdc2535c5f69416d8', NULL, '2022-06-21 16:39:37', '2022-06-21 16:39:37'),
(25, 'Originals: \"Tabrak Aturan\", Jadilah Pemenang', 'Adam M. Grant, Mursid Wijanarko', 2017, 'Lifestyle', 'Pegawai penjualan yang menggunakan Firefox atau Chrome lebih sukses daripada yang menggunakan Internet Explorer atau Safari.', 'https://covers.zlibcdn2.com/covers299/books/1b/91/5b/1b915bc503b91270f1d23170bba45229.jpg', 'https://id.id1lib.org/dl/4982975/ee6113?dsource=recommend', NULL, '2022-06-21 21:31:10', '2022-06-21 21:31:10'),
(26, 'Bulan', 'Tere Liye', 2015, 'Novel', 'Masih menceritakan petualangan dari tiga sekawan, pada novel Bulan ini dikisahkan Ali, Seli, dan Raib yang dibawa oleh Miss Selena dan Av untuk melakukan diplomasi di Klan Matahari. Tujuannya adalah untuk membuka portal atau gerbang yang sudah ribuan tahun lamanya ditutup.', 'https://covers.zlibcdn2.com/covers299/books/4f/8a/e9/4f8ae90537f720ede6aeffdcdcc7fd55.jpg', 'https://swab.zlibcdn.com/dtoken/c680f5a80b525b6a346055ea1bd891e7', NULL, '2022-06-21 21:34:52', '2022-06-21 23:05:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2014_10_12_000000_create_users_table', 1),
(19, '2014_10_12_100000_create_password_resets_table', 1),
(20, '2019_08_19_000000_create_failed_jobs_table', 1),
(21, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `book`
--
ALTER TABLE `book`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
